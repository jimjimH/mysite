<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Api\LoginController@register')->name('register');
Route::post('/login', 'Api\LoginController@login')->name('login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/logout', 'Api\LoginController@logout')->name('logout');
    Route::post('/checklogin', 'Api\LoginController@checkLogin')->name('checkLogin');
    Route::post('/profile', 'Api\LoginController@profile')->name('profile');
});
