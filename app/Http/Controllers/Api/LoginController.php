<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'email' => 'required|email',
            'password' => 'required|string|max:50',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $newUser = new User();
        $newUser->name = $request->input('name');
        $newUser->email = $request->input('email');
        $newUser->password = bcrypt($request->input('password'));
        try {
            $newUser->save();
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'DB save failed...'], 500);
        }

        return response()->json(['success' => 'Successfully created a new user !'], 201);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|max:50',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        //嘗試驗證會員資料，驗證成功則產生token並回傳
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $token = Auth::user()->createToken('user_token')->accessToken;
            return response()->json(['token' => $token], 200);
        } else {
            return response()->json(['error' => 'User is unauthorised.'], 401);
        }
    }

    public function logout()
    {
        /* 在寫這邊的時候有卡住一陣子：所幸靠著google終於了解他的意思，
        原來是trait HasApiTokens內早已定義了hasMany relationship between Laravel\Passport\Token and models using the trait
        參考網站:
        https://stackoverflow.com/questions/42851676/how-to-invalidate-all-tokens-for-an-user-in-laravel-passport
        https://github.com/laravel/passport/blob/2.0/src/HasApiTokens.php */

        $userToken = Auth::user()->token();
        $userToken->revoke();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function checkLogin()
    {
        if (Auth::check()) {
            return response()->json(['sucess' => Auth::user()->name . ' is already login.'], 200);
        } else {
            return response()->json(['error' => 'User is not login'], 200);
        }
    }

    public function profile()
    {
        $user = Auth::user();
        if (isset($user)) {
            return response()->json(['sucess' => $user], 200);
        } else {
            return response()->json(['error' => 'User is unauthorised'], 200);
        }
    }
}
